---
title: about
date: 2020-09-14 12:16:26
---
## Fachrizal Fahmy

+62 878 45341633  
fachrizalfahmy@gmail.com  
https://gitlab.com/adheizal
https://www.linkedin.com/in/fachrizalfahmy/  

## Employment

- DevOps Engineer, Jubelio https://jubelio.com 2021 - Present.
- Operations Support Manager, PT. Boer Technology https://btech.id Jan 2020 - 2021.
- Talent Manager NolSatu, NolSatu https://nolsatu.id Mar 2019 - Jan 2020.
- Senior Cloud Engineer, PT. Boer Technology https://btech.id Sep 2018 - 2021.

## Education

- Informatics Engineering, Universitas Amikom Yogyakarta, 2013-2017 3.26/4.00.

## Additional experience

- Speaker Indonesia Open Infrastructure Day , OpenStack, Nov 2019.

## Languages and technologies

- Automation: Ansible, Terraform
- CI/CD: Gitlab-CI, Jenkins
- Cloud: AWS, GCE, OpenStack
- Hardware/Appliances: Server, Storage, Router, Switch
- Orchestrator: Kubernetes
- Container: Docker
- Logging & Monitoring: Prometheus, ELK
- Network & Storage: Ceph, OpenIO, Minio, Varnish, Apache Traffic Control

## References

- Utian Ayuba. Executive Director of Btech. utian@btech.id.
