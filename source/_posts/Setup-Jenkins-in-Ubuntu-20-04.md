---
title: Setup Jenkins in Ubuntu 20.04
date: 2020-09-22 10:50:30
tags:
---


1. Add repository Jenkins and Install Jenkins
```bash
$ sudo wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
$ sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
$ sudo apt update
$ sudo apt install openjdk-11-jdk-headless jenkins -y
```
2. Start Jenkins
```bash
$ sudo systemctl enable --now jenkins
$ sudo systemctl status jenkins
```
3. Setting Up Jenkins
```bash
$ sudo ss -nl |grep 8080
$ cat /var/lib/jenkins/secrets/initialAdminPassword
```
4. Edit default configuration Jenkins
```bash
$ sudo vim /etc/default/jenkins
...
JENKINS_ARGS="--webroot=/var/cache/$NAME/war --httpPort=$HTTP_PORT --httpListenAddress=127.0.0.1"
...

$ sudo systemctl restart jenkins
$ sudo systemctl status jenkins
```
5. Setup Nginx for Proxy
```bash
$ sudo apt install nginx certbot -y
$ sudo systemctl stop nginx
$ sudo certbot certonly -d jenkins.goreng.cc --register-unsafely-without-email
$ sudo vim /etc/nginx/sites-available/jenkins.goreng.cc
...
server {
    listen                  443 ssl http2 ipv6only=on;
    listen                  [::]:443 ssl http2;
    server_name             jenkins.goreng.cc;

    access_log		    /var/log/nginx/jenkins.access.log;
    error_log		    /var/log/nginx/jenkins.error.log;

    # SSL
    ssl_certificate         /etc/letsencrypt/live/jenkins.goreng.cc/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/jenkins.goreng.cc/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/jenkins.goreng.cc/chain.pem;

    location / {
	include /etc/nginx/proxy_params;
        proxy_pass http://localhost:8080;
	proxy_read_timeout  90s;
	proxy_redirect      http://localhost:8080 https://jenkins.goreng.cc.com;
    }
}

# subdomains redirect
server {
    listen                  443 ssl http2;
    listen                  [::]:443 ssl http2;
    server_name             *.jenkins.goreng.cc;

    # SSL
    ssl_certificate         /etc/letsencrypt/live/jenkins.goreng.cc/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/jenkins.goreng.cc/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/jenkins.goreng.cc/chain.pem;
    return                  301 https://jenkins.goreng.cc$request_uri;
}

# HTTP redirect
server {
    listen      80;
    listen      [::]:80;
    server_name .jenkins.goreng.cc;

    location / {
        return 301 https://jenkins.goreng.cc$request_uri;
    }
}
...

$ sudo ln -s /etc/nginx/sites-available/jenkins.goreng.cc /etc/nginx/sites-enabled/
$ sudo systemctl start nginx
```
6. Access jenkins via Browser
```bash
https://jenkins.goreng.cc
```