---
title: Upgrade OpenStack Newton to Queens with Packstack
date: 2020-09-21 14:33:20
tags:
---


This Guide with Centos 7
1. Disable vault repository, return all repository configuration to default

2. Remove Package OpenStack Newton
```bash
$ sudo yum -y erase centos-release-openstack-newton.noarch centos-release-ceph
```
3. Update Package OS Centos
```bash
$ sudo yum -y upgrade centos-release
```
4. Install Package OpenStack Queens 
```bash
$ sudo yum -y install centos-release-openstack-queens.noarch
$ sudo yum -y upgrade
```
5. Backup file paf `paf-os-queens.txt`
```bash
$ cp paf-os-queens.txt paf-os-queens-backup.txt
```
6. Edit file paf `paf-os-queens.txt`
```bash
$ vim paf-os-queens.txt
...
CONFIG_KEYSTONE_API_VERSION=v3
...
```
7. Repackstack
```bash
$ sudo -i
# packstack --gen-answer-file=paf-os-queens.txt
```

##### If you get error like this
```bash
Error: /Stage[main]/Glance::Db::Sync/Exec[glance-manage db_sync]: Failed to call refresh: glance-manage  db_sync returned 1 instead of one of [0]
```
8. Update visibility glance in Database
```bash
# mysql -u root -p
mysql> show databases;
mysql> use glance;
mysql> show tables;
mysql> update images set visibility='private' where visibility is null;
```
9. Repackstack again
```bash
# packstack --gen-answer-file=paf-os-queens.txt
```
##### and If you get error like this 
```bash
Error: Execution of '/usr/bin/nova-manage cell_v2 create_cell --name default --verbose' returned 2: The specified transport_url and/or database_connection combination already exists for another cell with uuid 6d8d8cd9-eaa2-4d8c-9243-6c3d1ceff65b.
```
10. Sync Database nova with `nova-manage`
```bash
# nova-manage cell_v2 list_cells
# nova-manage cell_v2 list_hosts
# nova-manage cell_v2 simple_cell_setup
# su -s /bin/sh -c "nova-manage api_db sync" nova
# nova-manage cell_v2 update_cell --cell_uuid 6d8d8cd9-eaa2-4d8c-9243-6c3d1ceff65b --name default
```
Example of correct display for output :
```bash
# nova-manage cell_v2 list_cells
+---------+--------------------------------------+---------------------------------------+--------------------------------------------------+
|   Name  |                 UUID                 |             Transport URL             |               Database Connection                |
+---------+--------------------------------------+---------------------------------------+--------------------------------------------------+
|  cell0  | 00000000-0000-0000-0000-000000000000 |                 none:/                | mysql+pymysql://nova:****@"IP_CONTROLLER"/nova_cell0 |
| default | 3d87b051-1af7-4596-8b2c-a9c4d6099f2d | rabbit://guest:****@"IP_CONTROLLER":5672/ |    mysql+pymysql://nova:****@"IP_CONTROLLER"/nova    |
+---------+--------------------------------------+---------------------------------------+--------------------------------------------------+

# nova-manage cell_v2 list_hosts
+-----------+--------------------------------------+--------------+
| Cell Name |              Cell UUID               |   Hostname   |
+-----------+--------------------------------------+--------------+
|  default  | 3d87b051-1af7-4596-8b2c-a9c4d6099f2d | compute |
+-----------+--------------------------------------+--------------+
```
If there are cell that are not necessary, you can delete them.

11. Repackstack again
```bash
# packstack --gen-answer-file=paf-os-queens.txt
```
12. Post Deploy in Node Controller
- Update Metadata DHCP Agent
```bash
# crudini --set /etc/neutron/dhcp_agent.ini DEFAULT enable_isolated_metadata True
# systemctl restart neutron-dhcp-agent
# systemctl status neutron-dhcp-agent
```
13. Post Deploy in Node compute
- Enable service `virtlogd`
```bash
# systemctl status virtlogd
# systemctl enable virtlogd
# systemctl restart virtlogd
# systemctl status virtlogd
```
- Set Proxy Client
```bash
# crudini --set /etc/nova/nova.conf vnc vncserver_proxyclient_address "IP_COMPUTE"
# systemctl restart openstack-nova-compute
# systemctl status openstack-nova-compute
```
14. Check log service neutron-dhcp-agent.service. if you encounter an error like this:
```bash
2018-11-15 01:57:49.747 816 ERROR neutron.agent.linux.utils [req-991473ae-d745-47bb-bcad-2e2e1ece9d49 - - - - -] Rootwrap error running command: ['kill', '-9', '31985']: RemoteError: 
```
You can manually kill the PID process above and restart service neutron
```bash
# kill -9 31985
# systemctl restart neutron-dhcp-agent.service neutron-metadata-agent.service
```