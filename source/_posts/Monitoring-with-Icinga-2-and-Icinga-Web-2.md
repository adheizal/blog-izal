---
title: Monitoring with Icinga 2 and Icinga Web 2
date: 2021-01-18 09:32:41
tags:
---

I user this specification for this guide, detail VM :
- vm Icinga
  - CPU     : 2 core
  - Ram     : 4 GB
  - IP      : 10.1.0.12
  - OS      : CentOS 7
  - Disk    
    - vda   : 10GB

### Setup Icinga 2
1. Package Repositories 
```bash
$ sudo yum install epel-release -y
$ sudo yum install https://packages.icinga.com/epel/icinga-rpm-release-7-latest.noarch.rpm -y
$ sudo yum install https://repo.ius.io/ius-release-el7.rpm -y
```
2. Install Icinga2 and Plugin
```bash
$ sudo yum updatate -y
$ sudo yum install vim git wget -y
$ sudo yum install icinga2 nagios-plugins-all -y
$ sudo systemctl enable --now icinga2
```
3. SeLinux (Opsional if SeLinux enable)
```bash
$ sudo yum install icinga2-selinux -y
```
4. Syntax Highligh (Opsional)
```bash
$ sudo yum install vim-icinga2 -y
# Test it
$ vim /etc/icinga2/conf.d/templates.conf
```
5. Setup Database
```bash
$ sudo yum install mariadb104-server mariadb104 icinga2-ido-mysql -y
$ sudo systemctl enable --now mariadb
$ mysql_secure_installation
$ mysql -uroot -p
---
> CREATE DATABASE icinga;
> GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON icinga.* TO 'icinga'@'localhost' IDENTIFIED BY 'icinga';
> quit
---
$ mysql -uroot -p icinga < /usr/share/icinga2-ido-mysql/schema/mysql.sql
$ icinga2 feature enable ido-mysql
$ sudo systemctl restart icinga2
```
6. Configure webserver
```bash
$ sudo yum install httpd -y
$ sudo systemctl enable --now httpd
```
7. Setting Up Icinga 2 REST API
```bash
$ icinga2 api setup
# Edit file api-user.conf
$ sudo vim /etc/icinga2/conf.d/api-users.conf
---
object ApiUser "icingaweb2" {
  password = "fkvXpf9QOnY="
  permissions = [ "status/query", "actions/*", "objects/modify/*", "objects/query/*" ]
}
---
$ sudo systemctl restart icinga2
```
### Setup Icinga Web 2
1. Package Repositories 
```bash
$ sudo yum install centos-release-scl
```
2. Install Icinga Web 2 
```bash 
$ sudo yum install icingaweb2 icingacli icingaweb2-selinux -y
```
3. Setup PHP-FPM
```bash
$ sudo yum install rh-php73-php-mysqlnd rh-php73-php-imagick -y
$ sudo systemctl enable --now rh-php73-php-fpm
```
4. Preparing Web Setup 
```bash
$ icingacli setup token create
$ mysql -uroot -p
---
> CREATE DATABASE icingaweb2;
> GRANT ALL ON icingaweb2.* TO icingaweb2@localhost IDENTIFIED BY 'fkvXpf9QOnY';
---
$ sudo systemctl restart httpd
```

