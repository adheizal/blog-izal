---
title: MinIO for Private Storage S3
date: 2020-09-18 09:54:11
tags:
---


I user this specification for this guide, detail VM :

- vm-minio
    CPU : 1 core
    RAM : 2 GB
    IP : 192.168.1.6
    DISK :
    - vdb : 10 GB

### Installing MinIO Server

1. Format Disk For MinIO
```bash
$ sudo parted -s -a optimal -- /dev/vdb mklabel gpt
$ sudo parted -s -a optimal -- /dev/vdb mkpart primary 0% 100%
$ sudo parted -s -- /dev/vdb align-check optimal 1
$ sudo mkfs.ext4 /dev/vdb1
```
2. Mount Disk to Data
```bash
$ echo "/dev/vdb1 /data-01 ext4 defaults 0 0" | sudo tee -a /etc/fstab
$ sudo mkdir /data-01
$ sudo mount -a
```
3. Intall MinIO
```bash
$ wget https://dl.minio.io/server/minio/release/linux-amd64/minio
$ chmod +x minio
$ sudo mv minio /usr/local/bin
```
4. Create user MinIO
```bash
$ sudo useradd --system minio-user --shell /sbin/nologin
```
5. Create directories and chown it to minio-use
```bash
$ sudo mkdir -p /data/
$ sudo mkdir /etc/minio
$ sudo chown minio-user:minio-user /data/
$ sudo chown minio-user:minio-user /etc/minio
```
6. Create default config MinIO
```bash
$ sudo vi /etc/default/minio
...
# Volume to be used for Minio server.
MINIO_VOLUMES="/data"
# Use if you want to run Minio on a custom port.
MINIO_OPTS="--address :9000"
# Access Key of the server.
MINIO_ACCESS_KEY=BKIKJAA5BMMU2RHO6IBB
# Secret key of the server.
MINIO_SECRET_KEY=V7f1CwQqAcwo80UEIJEjc5gVQUSSx5ohQ9GSrr12
...
```
7. Run MinIO systemd
```bash
$ curl -O https://raw.githubusercontent.com/minio/minio-service/master/linux-systemd/minio.service
$ sudo mv minio.service /etc/systemd/system
$ sudo systemctl daemon-reload
$ sudo systemctl enable minio
$ sudo systemctl start minio
$ sudo systemctl status minio -l
```
8. Check Dashboard MinIO
* Access browser http://192.168.1.6:9000

9. Login use Access Key and Secret Key
```bash
$ cat /data/.minio.sys/config/config.json | grep accessKey
$ cat /data/.minio.sys/config/config.json | grep secretKey
```