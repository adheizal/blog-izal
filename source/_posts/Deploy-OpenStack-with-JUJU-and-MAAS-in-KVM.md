---
layout: layout
title: Deploy OpenStack with JUJU and MAAS in KVM
tags: []
categories: []
date: 2023-01-22 14:37:00
---


I will try to create guide Deploy Openstack Juju with MAAS in KVM, but first i will explaine What is Openstack ? What is JUJU ? What is MAAS ?

**OpenStack** is a free open standard cloud computing platform, mostly deployed as infrastructure-as-a-service (IaaS) in both public and private clouds where virtual servers and other resources are made available to users. The software platform consists of interrelated components that control diverse, multi-vendor hardware pools of processing, storage, and networking resources throughout a data center. Users either manage it through a web-based dashboard, through command-line tools, or through RESTful web services. 

**Juju** is an open source, application and service modelling tool from Ubuntu that helps you deploy, manage and scale your applications on any cloud. and 

**MAAS** is an open-source tool that lets you build a data centre from bare-metal servers. You can discover, commission, deploy, and dynamically reconfigure a large network of individual units. MAAS converts your hardware investment into a cohesive, flexible, distributed data centre, with a minimum of time and effort.

Now i use this specification for this guide :

| Name | Role | Spec |
| :---: | :---: | :---: |
| maas | MAAS & Juju Server | - 2vCPU</br> - 4GB RAM</br> - 40GB Disk</br> - 2 Ethernet |
| juju-ctrl | Controller | - 2vCPU</br> - 8GB RAM</br> - 40GB Disk</br> - 2 Disk (50GB)</br> - 2 Ethernet |
| cmpt1 | Compute 1 | - 2vCPU</br> - 4GB RAM</br> - 40GB Disk</br> - 2 Disk (50GB)</br> - 2 Ethernet |
| cmpt2 | Compute 2 | - 2vCPU</br> - 4GB RAM</br> - 40GB Disk</br> - 2 Disk (50GB)</br> - 2 Ethernet |
| cmpt3 | Compute 3 | - 2vCPU</br> - 4GB RAM</br> - 40GB Disk</br> - 2 Disk (50GB)</br> - 2 Ethernet |


#### Setup Maas
In Node Maas & Juju you need to setup repository maas and install Virsh
- Install Maas
```bash
$apt-add-repository ppa:maas/stable -y
$apt update
$apt install maas -y
$sudo maas createadmin
```
- Install Virsh
```bash
$ apt install libvirt-bin -y
$ sudo chsh -s /bin/bash maas
$ sudo su - maas
$ ssh-keygen -f ~/.ssh/id_rsa -N ''
```
- Testing remote KVM Node via Maas & Juju Node
```bash
$ virsh -c qemu+ssh://user@IP_KVM/system list --all
```
- Access MAAS GUI
```
http://IP_MAAS/MAAS
```

#### Configure In Maas GUI
- Configure Connectivity DNS
```
DNS Forwader
8.8.8.8
```
- Choose Source installation Linux Distribution
```
16.04 LTS (arm64)
18.04 LTS (arm64)
```
- Add SSH Keys for Ubuntu
    - SubnetClick on Subnet line fabric0set DNS on network
    - SubnetClick on VLAN line fabric0Take actionprovide DHCP
    - SubnetClick on Subnet line fabric1set DNS on network
    - SubnetClick on VLAN line fabric1Take actionprovide DHCP
- Add New Zone
- zones add zonename : zone1
- Add Pods
    - add pods
    - name : maas
    - Pod type : Virsh (virtual system)
```
Virsh address : virsh -c qemu+ssh://user@IP_KVM/system list --all
```
- Change Zone all machine in Maas
```
Machine configuration
- zone : zone 1
```
- Edit all interface machine to (Static Assign)

#### Setup Juju
- In Node Maas & Juju
```bash
$ sudo add-apt-repository -yu ppa:juju/stable
$ sudo apt update
$ sudo apt install juju -y
```
- Add Juju Cloud
```bash
$ juju add-cloud
- maas
- http://IP_MAAS/MAAS

$ juju add-credential
- ubuntu
- token MAAS
```
- Create Controller 
```bash
$ juju bootstrap maas maas-controller1 --config image-stream=daily --config enable-os-upgrade=false --default-model default --to lev-maas0.maas --bootstrap-series=bionic --credential ubuntu
```
- Add Machine
```bash
$ juju switch controller
$ juju add-machine -n 4 --series bionic --constraints zones=zone1
```
- Access Juju GUI
```bash
$ juju gui
https://IP_Controller:17070/gui/u/admin/controller
```

#### Configure In Juju GUI (Option 1)
- Use Controller Model
```
search 'openstack base'
move all service to machine maas
```
- Custom Configuration for ceph-osd, nova-compute, neutron-gateway
```
- ceph-osd
osd-devices (string) : /dev/vdb /dev/vdc

- nova-compute
virt-type (string) : qemu

- neutron-gateway
data-port (string) : br-ex:ens8 (adjust to the interface name to 2)

- commit change
commit

# In Terminal
$ juju status
```

#### Another Option to Deploy (Option 2)
- From bundel
```bash
$ juju deploy bundel.yaml --map-machines=existing
```
##### Wait until all application active
if there is an error on a charm we can run the following command to resolve that:
```bash
$ juju resolved neutron-gateway/0 --no-retry
```

#### Access OpenStack Dashboard
- Access horizon OpenStack and get a password OpenStack
```bash 
$ juju run --unit openstack-dashboard/0 'unit-get public-address'
$ juju run --unit keystone/0 leader-get admin_passwd
```
- Create openrc
```bash 
$ wget https://gitlab.com/adhelhgi/openstack-juju/raw/master/openrc
$ cd openstack-juju
$ source openrc
```
- Create External Network
```bash
$ wget https://gitlab.com/adhelhgi/openstack-juju/raw/master/neutron-ext-net-ksv3
$ chmod +x neutron-ext-net
$ ./neutron-ext-net --network-type flat \
   -g 10.71.71.1 -c 10.71.71.0/24 \
   -f 10.71.71.200:10.71.71.254 ext_net
```
- Create Internal Network
```bash
$ wget https://gitlab.com/adhelhgi/openstack-juju/raw/master/neutron-tenant-net-ksv3

$ chmod +x neutron-tenant-net-ksv3
$ ./neutron-int-net -t admin -r provider-router \
   -N 1.1.1.1 internal 10.10.10.0/24
```
- Create Flavor OpenStack
```bash
$ source openrc
$ openstack flavor create --public --ram 512 --disk 1 --ephemeral 0 --vcpus 1 m1.tiny
$ openstack flavor create --public --ram 1024 --disk 20 --ephemeral 40 --vcpus 1 m1.small
$ openstack flavor create --public --ram 2048 --disk 40 --ephemeral 40 --vcpus 2 m1.medium
$ openstack flavor create --public --ram 8192 --disk 40 --ephemeral 40 --vcpus 4 m1.large
$ openstack flavor create --public --ram 16384 --disk 80 --ephemeral 40 --vcpus 8 m1.xlarge
```

##### Debug
```bash
$ juju debug-log --include glance/1
$ juju ssh ceph-osd/0; pastebinit /var/log/juju/unit-ceph-osd-0.log
$ juju debug-log --replay --include-unit ceph-mon/0
```