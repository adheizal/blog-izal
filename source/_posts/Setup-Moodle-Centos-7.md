---
title: Setup Moodle Centos 7 
date: 2021-01-19 12:36:47
tags:
---

I user this specification for this guide, detail VM :
- Moodle
  - CPU : 2 core
  - Ram : 4 GB
  - IP : 10.1.0.11
  - OS : CentOS 7
  - Disk
    - vda : 10GB

# Setup Moodle
1. Package Repositories
```bash
$ sudo yum update -y
$ sudo yum install https://repo.ius.io/ius-release-el7.rpm https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm -y
$ sudo yum install vim git wget -y
```
2. Install Webserver
```bash
$ sudo yum install nginx -y
$ sudo systemctl enable --now nginx
```
3. Install and Configure Database
```bash
$ sudo yum install mariadb104-server mariadb104 -y
$ sudo systemctl enable --now mariadb
$ mysql_secure_installation 
---
Switch to unix_socket authentication [Y/n] y

...
Change the root password? [Y/n] y
New password: 
Re-enter new password: 
Password updated successfully!

...
Remove anonymous users? [Y/n] y
 ... Success!

...
Disallow root login remotely? [Y/n] y
 ... Success!

...
Remove test database and access to it? [Y/n] y
Reload privilege tables now? [Y/n] y
---
$ mysql -uroot -p
---
> CREATE DATABASE moodledb DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
> CREATE USER 'moodleuser'@'localhost' IDENTIFIED BY 'fkvXpf9QOnY';
> GRANT ALL PRIVILEGES ON moodledb.* TO 'moodleuser'@'localhost' IDENTIFIED BY 'fkvXpf9QOnY';
> FLUSH PRIVILEGES;
---
```
3. Install and Configure PHP-FPM
```bash
$ sudo yum install graphviz aspell php74-fpm php74-cli php74-pspell php74-common php74-gd php74-intl php74-mysqlnd php74-xml php74-xmlrpc php74-ldap php74-pecl-zip php74-json php74-opcache php74-mbstring php74-soap -y
$ sudo vim /etc/php.ini
---
cgi.fix_pathinfo=0
---
$ sudo vim /etc/php-fpm.d/www.conf
---
user = nginx
group = nginx

...
listen = /run/php-fpm/php-fpm.sock

...
listen.owner = nginx
listen.group = nginx
listen.mode = 0660

...
security.limit_extensions = .php

...
env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
---

$ sudo mkdir -p /var/lib/php/session/
$ sudo chown -R nginx:nginx /var/lib/php/session/
$ sudo systemctl enable --now php-fpm
$ sudo chown -R nginx:nginx /run/php-fpm/
```
4. Download and Configure Moodle
```bash
$ cd /app
$ git clone https://github.com/moodle/moodle.git
$ cd moodle/
$ git branch -a
$ git branch --track MOODLE_310_STABLE origin/MOODLE_310_STABLE
$ git checkout MOODLE_310_STABLE
$ mkdir -p /app/moodledata
$ sudo chown -R nginx:nginx /app/moodledata
$ sudo chmod 777 /app/moodledata
$ sudo chown -R nginx:nginx /app/moodle/
$ sudo chmod 777 /app/moodle/
```
5. Configure Nginx
```bash
$ sudo vim /etc/nginx/conf.d/moodle.conf
---
server {
    listen   80;
    server_name  10.1.0.11;

    # note that these lines are originally from the "location /" block
    root   /app/se-prac-test/sepractest/moodle;
    index index.php index.html index.htm;

    location / {
        try_files $uri $uri/ =404;
    }
    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /app/se-prac-test/sepractest/moodle;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
---
$ nginx -t
$ sudo systemctl restart nginx
```
6. Access Moodle
```
Access Moodle via browser http://10.1.0.11/install.php
```
- Choose Language Instalation, **Next**
<img no-lazy src="./moodle-10.png"/>
- Confirmation Path Moodle and Moodledata **Next**
<img no-lazy src="./moodle-01.png"/>
- Choose Database Used **Next**
<img no-lazy src="./moodle-02.png"/>
- Database Configuration, **Next**
```
Database Host : localhost
Database Name : moodledb
Database User : moodleuser
Database Password : fkvXpf9QOnY
Tables prefix : moodle_
Database Port : 3306
Unix Socket : /var/lib/mysql/mysql.sock
```
<img no-lazy src="./moodle-03.png"/>
- Moodle Copyright, **Next**
<img no-lazy src="./moodle-04.png"/>
- Server Check Plugin Moodle, **Next**
<img no-lazy src="./moodle-05.png"/>
- If there is an update regarding the Moodle Plugin, **Upgrade Moodle database now**
<img no-lazy src="./moodle-06.png"/>
- Process Upgrade Done, **Continue**
<img no-lazy src="./moodle-07.png"/>
- User Configuration, fill in as needed, **Continue and Save**
<img no-lazy src="./moodle-08.png"/>
- Try logging in using the user and password previously created
<img no-lazy src="./moodle-09.png"/>