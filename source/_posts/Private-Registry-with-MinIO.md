---
title: Private Registry with MinIO
date: 2020-09-15 19:28:55
tags:
---

I user this specification for this guide, detail VM :
- minio-demo  
  CPU : 1 core  
  RAM : 2 GB  
  IP : 192.168.100.6  
  DISK :   
  - vdb : 10 GB


### Installing MinIO Server

1. Format Disk For MinIO
```bash
$ sudo parted -s -a optimal -- /dev/vdb mklabel gpt
$ sudo parted -s -a optimal -- /dev/vdb mkpart primary 0% 100%
$ sudo parted -s -- /dev/vdb align-check optimal 1
$ sudo mkfs.ext4 /dev/vdb1
```
2. Mount Disk to Data
```bash
$ echo "/dev/vdb1 /data-01 ext4 defaults 0 0" | sudo tee -a /etc/fstab
$ sudo mkdir /data-01
$ sudo mount -a
```
3. Intall MinIO
```bash
$ wget https://dl.minio.io/server/minio/release/linux-amd64/minio
$ chmod +x minio
$ sudo mv minio /usr/local/bin
```
4. Create user MinIO
```bash
$ sudo useradd --system minio-user --shell /sbin/nologin
```

5. create directories and chown it to minio-use
```bash
$ sudo mkdir -p /data/
$ sudo mkdir /etc/minio
$ sudo chown minio-user:minio-user /data/
$ sudo chown minio-user:minio-user /etc/minio
```

6. Create default config MinIO
```bash
$ sudo vi /etc/default/minio
...
# Volume to be used for Minio server.
MINIO_VOLUMES="/data"
# Use if you want to run Minio on a custom port.
MINIO_OPTS="--address :9000"
# Access Key of the server.
MINIO_ACCESS_KEY=BKIKJAA5BMMU2RHO6IBB
# Secret key of the server.
MINIO_SECRET_KEY=V7f1CwQqAcwo80UEIJEjc5gVQUSSx5ohQ9GSrr12
...
```

7. Run MinIO systemd
```bash
$ curl -O https://raw.githubusercontent.com/minio/minio-service/master/linux-systemd/minio.service
$ sudo mv minio.service /etc/systemd/system
$ sudo systemctl daemon-reload
$ sudo systemctl enable minio
$ sudo systemctl start minio
$ sudo systemctl status minio -l
```

8. Check Dashboard MinIO
* access browser http://minio-demo.goreng.cc:9000

Login use Access Key and Secret Key
``` bash
$ cat /data/.minio.sys/config/config.json | grep accessKey
$ cat /data/.minio.sys/config/config.json | grep secretKey
```
9. Check Dashboard MinIO
* access browser http://minio-demo.goreng.cc:9000
* Create Bucket docker-registry

### Setup Private Registry

10. Setup Registry
- Create Password for registry
```bash
$ htpasswd -Bbn admin Rahasia > htpasswd
$ mkdir docker_auth
$ cp htpasswd docker_auth
```

- Create Custome Image Registry
```bash
$ mkdir minio && cd minio
$ vim Dockerfile
...
FROM registry:2
COPY config.yml /etc/docker/registry/config.yml
...

$ vim config.yml
...
version: 0.1
log:
    fields:
        service: registry
http:
    addr: :5000
storage:
    cache:
        layerinfo: inmemory
    s3:
        accesskey: 7SBGPQB4C46HJADDJJ93
        secretkey: FeQ1mSL3i++9O+rAayRF6WWjfLGqE+LSIDn7waV3
        region: us-east-1
        regionendpoint: https://minio-demo.goreng.cc
        bucket: docker-registry
        encrypt: false
        secure: true
        v4auth: true
        chunksize: 5242880
...

$ docker build -t minio .
```
- Create Docker-compose for Run Registry
```bash
$ cd
$ vim docker-compose.yml 
...
version: '3'
services:
  registry:
    image: minio:latest 
    restart: always
    ports:
      - "5000:5000"
    environment:
      - REGISTRY_AUTH=htpasswd
      - REGISTRY_AUTH_HTPASSWD_REALM="Registry Realm"
      - REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd
    volumes:
      - ./docker_auth:/auth 
      - ./data/docker-registry
...
$ docker-compose up -d
```

### Config Client for access push and pull 

11. Create daemon.json
```bash
$ sudo vim /etc/docker/daemon.json
...
{
  "insecure-registries" : ["minio-demo.goreng.cc:5000"]
}
...
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```

12. Login to Private Registry
```bash
$ docker login http://minio-demo.goreng.cc:5000
```
